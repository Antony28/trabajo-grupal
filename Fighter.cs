﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3 {
    class Fighter : Character {
        public Fighter(int exp) : base(exp) {
        }

        public override void ShowMenu() {
            base.ShowMenu();
            string str = Console.ReadLine();

            switch (str) {
                case "1":
                    Attack();
                    break;
            }

            ShowMenu();
        }
    }
}