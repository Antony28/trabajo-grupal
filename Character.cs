﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3 {
    abstract class Character {

        protected int exp = 0;
        public int level {
            get {
                int lvl = exp / 750;
                return lvl;
            }
        }

        public Character(int exp) {
            this.exp = exp;
        }

        public void AddExp(int exp) {
            this.exp = exp;
        }

        public virtual string GetInfo() {
            return "[\n level: " + level + ",\n exp: " + exp + "\n]";
        }

        public virtual void ShowMenu() {
            Console.WriteLine("ACTIONS");
            Console.WriteLine("=======");

            Console.WriteLine("  1 - Attack");
        }

        public void Attack() {
            Console.WriteLine(">Attack");
        }
    }
}