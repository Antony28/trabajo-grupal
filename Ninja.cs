﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// New Character bro! 
namespace ConsoleApp3
{
    class Ninja : Character
    {
        public Ninja(int exp) : base(exp)
        {
        }

        public override void ShowMenu()
        {
            base.ShowMenu();
            string str = Console.ReadLine();

            switch (str)
            {
                case "1":
                    Attack();
                    break;
                case "2":
                    Shuriken();
                    break;
                case "3":
                    Heal();
            }

            ShowMenu();
        }
    }
}

